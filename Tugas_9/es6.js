console.log("Nomor 1")
var golden = () => {
    console.log("this is golden!!")
}
golden()
console.log("-----------")

console.log("Nomor 2")
const newFunction = (firstName, lastName) => {
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: function() {
          const newFunction = `${firstName} ${lastName}`

          console.log(newFunction)
      }
    }
  }
  //Driver Code 
  newFunction("William", "Imoh").fullName()
  console.log("-----------")

  console.log("Nomor 3")
  const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
const { firstName, lastName, destination, occupation,spell } = newObject
  // Driver code
console.log(firstName, lastName, destination, occupation, spell)
console.log("-----------")

console.log("Nomor 4")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east]
//Driver Code
console.log(combined)
console.log("-----------")

console.log("Nomor 5")
const planet = "earth"
const view = "glass"
const before = `lorem ${view} dolor sit amet consectetur adipiscing elit ${planet} do eiusmod tempor incididunt ut labore er dolore magna aliqua. Ut enim ad minim veniam`
 
// Driver Code
console.log(before) 
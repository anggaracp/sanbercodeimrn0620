console.log("Poin A")
function AscendingTen(num) {
  var num2 = ""
  if (!num) {
    return -1
  }
  else {
    for (var i = 0; i < 10; i++, num++) {
      num2 = num2 + " " + num
    }
    return num2
  }
}
console.log(AscendingTen(11))
console.log(AscendingTen(21))
console.log(AscendingTen())
console.log("------------------")

console.log("Poin B")
function DescendingTen(num) {
  var num2 = ""
  if (!num) {
    return -1
  }
  else {
    for (var i = 0; i < 10; i++, num--) {
      num2 = num2 + " " + num
    }
    return num2
  }
}
console.log(DescendingTen(100))
console.log(DescendingTen(10))
console.log(DescendingTen())
console.log("------------------")

console.log("Poin C")
function ConditionalAscDesc(reference, check) {
  var number1 = ""
  var number2 = ""
  if (!reference && !check) {
    return -1
  }
  else if (reference && !check) {
    return -1
  }
  else {
    if ( check%2==1) {
      for(i = 0; i < 10; i++, reference++) {
        number1 = number1 + " " + reference
      }
      return number1
    }
    else {
      for(i = 0; i < 10; i++, reference--) {
        number2 = number2 + " " + reference
      }
      return number2
    }
  }
}
console.log(ConditionalAscDesc(20, 8)) // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)) // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)) // -1
console.log(ConditionalAscDesc()) // -1
console.log("------------------")

console.log("Poin D")
function ularTangga() {
  for (var x = 9; x >= 0; x--) {
    if (x%2==1) {
      for (var y = 9; y >= 0; y--) {
        process.stdout.write(y+1+x*10+" ")
    }
    
    process.stdout.write("\n")
  }
  else {
    for(var y = 1; y <= 10; y++) {
      process.stdout.write(y+x*10+" ")
    }
    process.stdout.write("\n")
  }
}
return " "
}
console.log(ularTangga())
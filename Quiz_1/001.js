console.log("Poin A")
function balikString(string) {
    var last = string.length - 1
    var test = ""
    while(last >= 0) {
        test = test + string[last]
        last--
    }
    return test
}
console.log(balikString("abcde"))
console.log(balikString("rusak"))
console.log(balikString("racecar"))
console.log(balikString("haji"))
console.log("-----------------")

console.log("Poin B")
function palindrome(string) {    
  var coba = string.length;
  for (i = 0; i < coba/2; i++) {
    if(string[i]!==string[coba-1-i]) {
      return false
    }
  }
  return true
}
console.log(palindrome("kasur rusak"))
console.log(palindrome("haji ijah"))
console.log(palindrome("nabasan"))
console.log(palindrome("nababan"))
console.log(palindrome("jakarta"))
console.log("-----------------")

function bandingkan(num1, num2) {
  if (!num1 && !num2) {return -1}
  else if (num1 && !num2) {return num1}
  else {
    if (num1 < 1 || num2 < 1) {
      return -1
    }
    else {
      if (num1 > num2) {
        return (num1)
      }
      else if (num2 > num1) {
        return (num2)
      }
      else if (num1 = num2) {
        return -1
      }
    }
  }
}
console.log(bandingkan(10, 15));
console.log(bandingkan(12, 12));
console.log(bandingkan(-1, 10));
console.log(bandingkan(112, 121));
console.log(bandingkan(1));
console.log(bandingkan());
console.log(bandingkan("15", "18"))

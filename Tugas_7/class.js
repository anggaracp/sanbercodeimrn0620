console.log("Nomor 1")
class Animal {
    constructor(name, legs, cold_blooded) {
        this.name = name
        this.legs = 4
        this.cold_blooded = false
    }
}
var sheep = new Animal("shaun")

console.log(sheep.name)
console.log(sheep.legs)
console.log(sheep.cold_blooded)
console.log("-----------")

class Frog extends Animal{
    constructor(name, legs, cold_blooded, jump) {
        super(name)
        this.legs = 4
        this.cold_blooded = true
        this.jump = "hop hop"
    }
}
var frog = new Frog("buduk")

console.log(frog.name)
console.log(frog.legs)
console.log(frog.cold_blooded)
console.log(frog.jump)
console.log("-----------")

class Ape extends Animal{
    constructor(name, legs, cold_blooded, yell) {
        super(name)
        this.legs = 2
        this.cold_blooded = false
        this.yell = "Auooo"
    }
}
var ape = new Ape("kera sakti")

console.log(ape.name)
console.log(ape.legs)
console.log(ape.cold_blooded)
console.log(ape.yell)
console.log("-----------")

console.log("Nomor 2")
class Clock {
    constructor({template}) {
        this.template = template
    }
    render() {
        var date = new Date()

        var hours = date.getHours()
        if (hours < 10) hours = "0" + hours

        var mins = date.getMinutes()
        if (mins < 10) mins = "0" + mins

        var secs = date.getSeconds()
        if (secs < 10) secs = "0" + secs

        var output = this.template
        .replace("h", hours)
        .replace("m", mins)
        .replace("s", secs)

        console.log(output)
    }

    stop() {
        clearInterval(this.timer)
    }

    start() {
        this.render()
        this.timer = setInterval(() => this.render(), 1000 )
    }
}

var clock = new Clock({template: "h:m:s"})
clock.start()
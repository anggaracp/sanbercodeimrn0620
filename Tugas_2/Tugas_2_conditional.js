//soal pertama
var nama = "Topson"
var peran = "Penyihir"

if (nama == "" && peran == "") {
    console.log("Nama harus diisi!")    
} else if (nama == "Topson" && peran == "") {
    console.log("Halo", nama, ", pilih peranmu untuk bermain game!")
} else if (nama == "Topson" && peran == "Penyihir") {
    console.log("Halo", nama, ",peranmu dalam game ini adalah Penyihir")
} else if (nama == "Topson" && peran == "Werewolf") {
    console.log("Halo", nama, ",peranmu dalam game ini adalah Werewolf")
} else if (nama == "Topson" && peran == "Penjaga") {
    console.log("Halo", nama, ",peranmu dalam game ini adalah Penjaga")
}

//soal kedua
var hari = 2
var bulan = 6
var tahun = 2020

if (hari>=1 && hari<=31 && bulan>=1 && bulan<=12) {
    switch(bulan) {
        case 1: {console.log(hari, "Januari", tahun); break}
        case 2: {console.log(hari, "Februari", tahun); break}
        case 3: {console.log(hari, "Maret", tahun); break}
        case 4: {console.log(hari, "April", tahun); break}
        case 5: {console.log(hari, "Mei", tahun); break}
        case 6: {console.log(hari, "Juni", tahun); break}
        case 7: {console.log(hari, "Juli", tahun); break}
        case 8: {console.log(hari, "Agustus", tahun); break}
        case 9: {console.log(hari, "September", tahun); break}
        case 10: {console.log(hari, "Oktober", tahun); break}
        case 11: {console.log(hari, "November", tahun); break}
        case 12: {console.log(hari, "Desember", tahun); break}
    }
} else (console.log("Input salah"))


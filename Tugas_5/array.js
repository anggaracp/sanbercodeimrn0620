console.log("Nomor 1")
function range(startNum, finishNum) {
    if (!startNum||!finishNum) {
        return -1
    }
    else if (startNum!=="" && finishNum!=="") {
        var number = []
        if (startNum < finishNum) {
            for(let i = startNum; i <= finishNum; i++) {
   number.push(i)
            }
        }
        else if (startNum >= finishNum) {
            for(let i = finishNum; i <= startNum; i++) {
                number.unshift(i)
            }
        }
    }
    return number
}
console.log(range(1, 10))
console.log(range(1))
console.log(range(11,18))
console.log(range(54, 50))
console.log(range())
console.log("------------------------")

console.log("Nomor 2")
function rangeWithStep(startNum, finishNum, step) {
    var number = []
    number[0] = startNum
    var test = 0
    if (startNum < finishNum) {
        while (startNum <= finishNum) {
            number[test] = startNum
            startNum = startNum + step
            test++
        }
    }
        else if (startNum > finishNum) {
            while (startNum >= finishNum) {
                number[test] = startNum
                startNum = startNum - step
                test++
            }
        }
        return number
}
console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))
console.log("------------------------")

console.log("Nomor 3")
function sum(x,y,step) {
    var number = []
    number[0] = x
    var test = 0
    if (x && !y) {
        return x
    }
    sum1 = 0
    if (!step) {
        step = 1
    }
    if (x < y) {
        while (x <= y) {
            number[test] = x
            sum1 = sum1 + number[test]
            x = x + step
            test++
        }
    }
    else if ( x > y) {
        while (x >= y) {
            number[test] = x
            sum1 = sum1 + number[test]
            x = x - step
            test++
        }
    }
    return sum1
}

console.log(sum(1,10))
console.log(sum(5, 50, 2))
console.log(sum(15,10))
console.log(sum(20, 10, 2))
console.log(sum(1))
console.log(sum())
console.log("------------------------")

console.log("Nomor 4")
function dataHandling(x) {
    for (var x = 0; x <= 3; x++) {
        console.log("Nomor ID : \t" + input[x][0])
        console.log("Nama Lengkap : \t" + input[x][1])
        console.log("TTL : \t" + input[x][2] + input[x][3])
        console.log("Hobi : \t" + input [x][4])
        console.log("\n")
    }
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung ", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan ", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon ", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura ", "6/4/1970", "Berkebun"]
]
dataHandling()
console.log("------------------------")

console.log("Nomor 5")
function balikKata(string) {
    var last = string.length - 1
    var test = ""
    while(last >= 0) {
        test = test + string[last]
        last--
    }
    return test
}
console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar"))
console.log(balikKata("I am Sanbers"))
console.log("------------------------")

console.log("Nomor 6")
function dataHandling1(input) {
    input.splice(1,2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung")
    input.splice(4,2, "Pria", "SMA Internasional Metro")
    console.log(input)

    var month = input[3].split("/")
    switch(month[1]) {
        case "01" : {console.log("Januari"); break}
        case "02" : {console.log("Februari"); break}
        case "03" : {console.log("Maret"); break}
        case "04" : {console.log("April"); break}
        case "05" : {console.log("Mei"); break}
        case "06" : {console.log("Juni"); break}
        case "07" : {console.log("Juli"); break}
        case "08" : {console.log("Agustus"); break}
        case "09" : {console.log("September"); break}
        case "10" : {console.log("Oktober"); break}
        case "11" : {console.log("November"); break}
        case "12" : {console.log("Desember"); break}
        default: {console.log("Jumlah bulan hanya 12!"); break}
    }
    var date = month.join("-")
    month.sort((x,y) => y - x)
    console.log(month)
    console.log(date)

    var name = input[1]
    var name1 = name.slice(0,14)
    console.log(name1)
}
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "membaca"]
dataHandling1(input)
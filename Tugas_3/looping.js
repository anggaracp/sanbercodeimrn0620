var deret = 0
console.log("LOOPING PERTAMA")
while(deret < 20) { 
  deret += 2
  console.log(deret, " - I love coding")
}
console.log("-------------------------")

var deret2 = 22
console.log("LOOPING KEDUA")
while(deret2 > 0) {
    deret2 -= 2
    console.log(deret2, " - I will become a mobile developer")
}
console.log("-------------------------")

console.log("LOOPING dengan for")
for(deret = 1; deret <= 20; deret++) {
  if(deret %3 == 0 && deret %2 == 1) {
    console.log(deret," - I Love Coding")
  }
  else if(deret %2 == 1) {
    console.log(deret, " - Santai")
  }
  else {console.log(deret, " - Berkualitas")}
}
console.log("-------------------------")

console.log("LOOPING persegi panjang")
string = "########";
for(x = 1; x < 5; x++) {
  console.log(string)
}
console.log("-------------------------")

console.log("LOOPING tangga")  
string2 = "";
for(x = 1; x < 7; x++) {
  for(y = 1; y < 7; y++) {
    if(x >= y) {
      string2 = string2+"#";
    }
  }
  string2 = string2+"\n"
}
console.log(string2)
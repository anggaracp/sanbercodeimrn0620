//Soal 1
var now = new Date()
var thisYear = now.getFullYear()

function arrayToObject(arr) {
    if(arr.length === 0){
      return ""
    }
    else{
    for(let i=0;i<arr.length;i++){
        var object = {
            firstName : arr[i][0],
            lastName : arr[i][1],
            gender : arr[i][2],
            age : thisYear - arr[i][3]
        }
        if(!arr[i][3] || arr[i][3]> thisYear){
            object.age = "Invalid Birth Year"
        }
        console.log(i+1+". "+object.firstName+" "+object.lastName+": ")
        console.log(object)
    }
    }

}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975],["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
    // Error case 
arrayToObject([]) // ""

//Pembatas
console.log("--------------------------------------------------")
//Soal 2
function shoppingTime(memberId, money) {
    if(!memberId){
      return "Mohon maaf, Toko X hanya berlaku untuk member saja"
    }
    else if(money<50000){
      return "Mohon maaf, Uang tidak cukup"
    }
    else{
      var i = 0
      var object = {
        memberId: memberId,
        money: money,
        listPurchased: [],
        changeMoney : money
      }
      if(money>=1500000){
        object.listPurchased[i] = "Sepatu Stacattu"
        money-= 1500000
        i++
      }
      if(money>=500000){
        object.listPurchased[i] = "Baju Zoro"
        money -= 500000
        i++
      }
      if(money>=250000){
        object.listPurchased[i] = "Baju H&N"
        money -= 250000
        i++
      }
      if(money>=175000){
        object.listPurchased[i] = "Sweater Uniklooh"
        money -= 175000
        i++
      }
      if(money>=50000){
        object.listPurchased[i] = "Casing Handphone"
        money -= 50000
        i++
      }
      object.changeMoney = money
      return object
    }
    
    
  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //   { memberId: '1820RzKrnWn08',
  //   money: 2475000,
  //   listPurchased:
  //    [ 'Sepatu Stacattu',
  //      'Baju Zoro',
  //      'Baju H&N',
  //      'Sweater Uniklooh',
  //      'Casing Handphone' ],
  //   changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  // { memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
//Pembatas
console.log("--------------------------------------------------")
//Soal 3
function naikAngkot(arrPenumpang) {
  var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  var arr = []
  if(arrPenumpang.length === 0){
    return arr
  }

  else{
  for(i=0;i<2;i++){
    var indexTujuan = rute.indexOf(arrPenumpang[i][2])
    var indexDari = rute.indexOf(arrPenumpang[i][1])
    var harga = (indexTujuan - indexDari)*2000
    var obj ={
      penumpang : arrPenumpang[i][0],
      naikDari : arrPenumpang[i][1],
      tujuan : arrPenumpang[i][2],
      bayar : harga
    }
    arr[i]=obj
  }
  }
  return arr
}
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
console.log(naikAngkot([])); //[]
import React, { Component } from 'react'
import { View, StyleSheet,FlatList,Platform, Text,Image, TouchableOpacity, SnapshotViewIOS } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';


export default class App extends Component {
    render() {
      return (
        <View style={styles.container}>
                <View style={styles.logo}>
                    <Text style={styles.judul}>MyApps</Text>
                    <Image source={require('./Images/logo1.png')} style={{width:150,height:150,borderRadius:10}} />
                    <View style={styles.isi}>
                        <View style={styles.isis}>
                            <Text style={styles.isiText}>Username</Text>
                            <View style={styles.kolomIsi}></View>
                        </View>
                        <View style={styles.isis}>
                            <Text style={styles.isiText}>Password</Text>
                            <View style={styles.kolomIsi}></View>
                        </View>
                    </View>
                    <View style={styles.login}>
                        <Text style={styles.loginText}>Log In</Text>
                    </View>
                </View>
        </View>
      )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
      justifyContent:'space-around',
    },
    judul:{
        fontSize: 64,
        color: '#FF7979',
        marginBottom: 10
    },
    logo:{
        alignItems:'center',
        paddingBottom:20
    },
    isi:{
        paddingTop:100,
        paddingBottom:60
    },
    isis:{
        paddingTop: 20
    },
    isiText:{
        fontSize: 20,
        color : '#FF7979',
        alignSelf:'flex-start',
        marginBottom: 30
    },
    kolomIsi:{
        height:2,
        backgroundColor:'#000000',
        alignSelf:'center',
        width:300
    },
    login:{
        borderRadius: 10,
        backgroundColor: '#FF7979',
        height: 50,
        width: 300,
        justifyContent: 'center'
    },
    loginText:{
        fontSize: 30,
        color: 'white',
        textAlign: 'center'
    }
  });
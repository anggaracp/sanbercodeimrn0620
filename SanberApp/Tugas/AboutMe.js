import React, { Component } from 'react'
import { View, StyleSheet,FlatList,Platform, Text,Image, TouchableOpacity, SnapshotViewIOS } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';


export default class App extends Component {
    render() {
      return (
        <View style={styles.container}>
            <View style={styles.button}>
                        <Text style={styles.buttonText}>X</Text>
                    </View>
                <View style={styles.foto}>
                    <Image source={require('./Images/foto.png')} style={{width:200,height:200,borderRadius:90}} />
                    <View style={styles.isi}>
                        <View style={styles.isi1}>
                            <Text style={styles.isiText}>Name</Text>
                            <Text style={styles.isiContent}>Muhammad Topson</Text>
                            <View style={styles.kolomIsi}></View>
                        </View>
                        <View style={styles.isi1}>
                            <Text style={styles.isiText}>Facebook</Text>
                            <Text style={styles.isiContent}>Muh Topson</Text>
                            <View style={styles.kolomIsi}></View>
                        </View>
                        <View style={styles.isi1}>
                            <Text style={styles.isiText}>Instagram</Text>
                            <Text style={styles.isiContent}>MTopson.numbawan</Text>
                            <View style={styles.kolomIsi}></View>
                        </View>
                        <View style={styles.isi1}>
                            <Text style={styles.isiText}>Twitter</Text>
                            <Text style={styles.isiContent}>@IndieBoys</Text>
                            <View style={styles.kolomIsi}></View>
                        </View>
                        <View style={styles.isi1}>
                            <Text style={styles.isiText}>Github</Text>
                            <Text style={styles.isiContent}>@MTopson</Text>
                            <View style={styles.kolomIsi}></View>
                        </View>
                    </View>
                    
                </View>
        </View>
      )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
      justifyContent:'space-around',
    },
    button:{
        borderRadius: 90,
        marginLeft: 10,
        backgroundColor: '#FF7979',
        height: 50,
        width: 50,
        justifyContent: 'center'
    },
    buttonText:{
        fontSize: 40,
        color: 'black',
        textAlign: 'center'
    },
    judul:{
        position: 'absolute',
        left: 0,
        marginBottom: 10
    },
    foto:{
        alignItems:'center',
        paddingBottom:20
    },
    isi:{
        paddingTop:80,
        paddingBottom:10
    },
    isi1:{
        paddingTop: 10
    },
    isiText:{
        fontSize: 20,
        color : '#FF7979',
        alignSelf:'flex-start'
    },
    isiContent:{
        fontSize: 30,
        color: '#000000',
        alignSelf: 'flex-start',
    },
    kolomIsi:{
        height:2,
        backgroundColor:'#000000',
        alignSelf:'center',
        width:300
    },
  });
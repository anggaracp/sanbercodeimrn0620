import React, { Component } from 'react'
import { View, StyleSheet,FlatList,Platform, Text,Image, TouchableOpacity, SnapshotViewIOS } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';


export default class App extends Component {
    render() {
      return (
        <View style={styles.container}>
            <View style={styles.header}>
                        <Text style={styles.headerText}>Knowledge</Text>
                        <View style={styles.button}>
                            <Text styles={styles.buttonText}>About Me</Text>
                        </View>
                        </View>
            <Text style={{fontSize:45,marginLeft:10,marginTop:10,textAlign:'left',color:'black'}}>Hello Jennie,</Text>
            <Text style={styles.subTitle}>Programming Language</Text>
            <View style={{height:2,backgroundColor:'#000000'}}></View>
            <View style={styles.boxIsi}>
                <Image source={require('./Images/logo8.png')} style={styles.logo}/>
                <Text style={styles.judul}>Kotlin</Text>
                <Text style={styles.subJudul}>Intermediate</Text>
            </View>
            <View style={styles.boxIsi2}>
                <Image source={require('./Images/logo4.png')} style={styles.logo}/>
                <Text style={styles.judul}>JavaScript</Text>
                <Text style={styles.subJudul}>Basic</Text>
            </View>
            <Text style={styles.subTitle}>Framework</Text>
            <View style={{height:2,backgroundColor:'#000000'}}></View>
            <View style={styles.boxIsi}>
                <Image source={require('./Images/logo2.png')} style={styles.logo}/>
                <Text style={styles.judul}>React Native</Text>
                <Text style={styles.subJudul}>Basic</Text>
            </View>
            <Text style={styles.subTitle}>Tools</Text>
            <View style={{height:2,backgroundColor:'#000000'}}></View>
            <View style={styles.boxIsi}>
            <Image source={require('./Images/logo9.png')} style={styles.logo}/>
                <Text style={styles.judul}>Android Studio</Text>
                <Text style={styles.subJudul}>Advance</Text>
            </View>
        </View>
      )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    header:{
        backgroundColor: '#FF7979',
        height: 50,
        alignContent: 'center'
    },
    button:{
        position:'absolute',
        alignSelf:'flex-end',
        alignItems:'center',
        top:25,
        borderRadius: 15,
        backgroundColor: '#B4B4B4',
        height: 25,
        width: 80,
    },
    headerText:{
        paddingTop: 20,
        fontSize: 20,
        color: 'white',
        alignSelf:'center'
    },
    buttonText:{
        fontSize: 25,
        color: 'black',
        alignSelf: 'center',    
    },
    subTitle:{
        marginBottom:10,
        fontSize: 25,
        color: 'black',
        marginLeft: 10,
        marginTop: 5
    },
    boxIsi2:{
        position:'absolute',
        right:50,
        top:185,
        borderRadius:15,
        width:150,
        height:150,
        backgroundColor:'#C7CEEA'
    },
    boxIsi:{
        marginTop:10,
        marginLeft:10,
        borderRadius:15,
        width:150,
        height:150,
        backgroundColor:'#E2F0CB'
    },
    logo:{
        marginTop:20,
        width:50,
        height:50,
        alignSelf:'center'
    },
    judul:{
        marginTop:5,
        fontSize:20,
        color:'black',
        alignSelf:'center'
    },
    subJudul:{
        alignSelf:'center',
        fontSize:20,
        color:'grey'
    },
    foto:{
        alignItems:'center',
        paddingBottom:20
    },
    isi:{
        paddingTop:80,
        paddingBottom:10
    },
    isi1:{
        paddingTop: 10
    },
    isiText:{
        fontSize: 20,
        color : '#FF7979',
        alignSelf:'flex-start'
    },
    isiContent:{
        fontSize: 30,
        color: '#000000',
        alignSelf: 'flex-start',
    },
    kolomIsi:{
        height:2,
        backgroundColor:'#000000',
        alignSelf:'center',
        width:300
    },
  });
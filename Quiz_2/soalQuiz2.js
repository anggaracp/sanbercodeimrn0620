console.log("Nomor 1")
class Score {
  constructor(object, points, email) {
    this.object = object
    this.points = []
    this.email = email
  }
  average(...nilai) {
    let total = 0
    for (let nilai1 of nilai) {
      total += nilai1
    }
    let avg = total / nilai.length
    return avg
  }
}
nilaiku = new Score()
console.log(nilaiku.average(70,80,90,60))
console.log("==========================================")

console.log("Nomor 2")
const data = [
  ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]

function viewScores(data, subject) {
  if (subject == "quiz-1") {
    var nilaike = 1    
  } else if (subject == "quiz-2") {
    var nilaike = 2
  } else {var nilaike = 3}
  for (var i = 1; i <= 4; i++) {
    var object = {
      email : data[i][0],
      subject : subject,
      points : data[i][nilaike],
    }
    console.log(object)
  }
  return""
}
viewScores(data, "quiz-1")
console.log("\n")
viewScores(data, "quiz-2")
console.log("\n")
viewScores(data, "quiz-3")
console.log("==========================================")

console.log("Nomor 3")
function recapScores(data) {
  for (var i = 1; i <= 4; i++) {
    score1 = (data[i][1] + data[i][2] + data[i][3]) / 3
    if (score1 > 70 && score1 <= 80){
      var predikat = "participant"
    } else if (score1 > 80 && score1 <= 90) {
      var predikat = "graduate"
    } else if (score1 > 90) {
      var predikat = "honour"
    }
    console.log(`${i}. Email : ${data[i][0]}\n Rata-rata : ${score1}\n Predikat : ${predikat}\n`)
  }
}

recapScores(data);
